#!C:/prgFiles/perl/bin/Perl.exe
#!/usr/bin/perl



# ------------------------------------------------------------------------------------
# filename: kHitsGenerator.pl
# author:   Frode Klevstul (frode@klevstul.com) (http://www.watashi.no/)
# started:  06.06.2005
# ------------------------------------------------------------------------------------
#
# Revision - Newest version at the top - - - - - - - - - - - - - - - - - - - - - - - -
# version:	v01_20111028
#			- Fixed bug so proxy sites where no servers found are logged as well.
#			- Minor output adjustments.
#			- Program exists if too many proxy fails occur in a row. This has been
#			  added to avoid the program running for too long in case a list of
#			  unavailable proxy servers are parsed ($maxSequencedProxyFails).
#			- Improved parsing for finding proxy servers on HTML pages.
#			- Implemented POST method for none-proxy mode as well.
# version:	v01_20111027
#			- Added timestamp to server status output
#			  (eg "TIMSTAMP : 54/198 : FAILED : 192.168.1.1:80")
# version:  v01_20111026
#			- Cleaner output to main log file (log_kHitsGenerator.log)
#			- Added own log file for logging of proxy sites. Shows number of servers found,
#			  and how many servers that worked successfully and how many that failed.
#           - Improved log counter for proxy reading, now shows sites and servers.
#			- Added log parameter for logging of variables in POST requests.
# version:  v01_20111024
#           - Added counter to log / show how many proxy sites (pages) that are read from file.
# version:  v01_20111023
#           - Moved list of proxy servers to own separate file in stead of as part of
#             this Perl program.
# version:  v01_20061205
#			- Added an option to avoid checking servers that has already failed.
# version:  v01_20050615
#			- Implemented lock-file functionality to prevent multiple programs running
#			  at the same time.
# version:  v01_20050615
#           - Added a "pre URL". If this is set this page will be loaded first. This
#             is to hide "cheating". (For example load the overview page before you vote.)
#           - Added option to set referer (HTTP Referrer - Note: referrer was misspelled
#             by a WWW dude. So it became the standard to write it the wrong way with two
#             'r' in stead of with one. But in this code I use "referer".)
#           - Remove sorting servers before saving.
# version:  v01_20050612
#           - Option for random start server, and random number of hits.
#           - Writing status to logfile.
# version:  v01_20050611
#           - Added support for distinct servers (logging of proxies used).
# version:  v01_20050606
#           - The first version of this program was made.
# ------------------------------------------------------------------------------------





# -------------------
# use
# -------------------
use LWP::Simple;
use LWP::UserAgent;
use HTTP::Request::Common qw(POST);
use HTTP::Response;
use strict;





# -------------------
# declarations / initializations
# -------------------
my @proxyLists;
my $actualHits;
my $userAgent = LWP::UserAgent->new;
my $request;
my @serversSuccess;
my @serversFailed;
my @serversAll;
my @proxyOverviewPagesFailed;
my $noProxyOverviewPages = 0;
my $noProxyServers = 0;
my $postParameterString;
my %proxyOverview;

# - - - - -
# Request method
#
# Values: 	'POST', 'GET'
# Note:		POST sending parameters in the body or GET defining parameters in the URL.
# - - - - -
my $requestMethod = "POST";

# - - - - -
# Start server (proxy)
#
# Values:	'random' or an integer.
# Note:		What proxy server to start using
# Example: 	If we find 2000 servers we can chose to start using server 1000 in stead of 0
# - - - - -
my $startServer = 0;
#$startServer = "random";

# - - - - -
# HTTP referer
#
# Note:		This will be the refering page to the target URL. Most likely this
#			should be same as "$preURL" if you want this to make any sense.
# - - - - -
my $httpReferer = "http://www.inmotionkitesurfing.com/vote-girl-kitesurfer-2011";

# - - - - -
# Pre URL
#
# Note:		This URL will be accessed first. Usefull when for example voting, so it looks like
#			a user loads a page before he actually votes. This should be the same as the HTTP
#			referer page to make sense. If "Pre URL" is set, this must return success when trying
#			to load the object. If not the "Target URL" won't be loaded.
# - - - - -
my $preUrl = "http://www.inmotionkitesurfing.com/vote-girl-kitesurfer-2011";

# - - - - -
# URL to request
#
# Note: 	If you're using POST request method the parameters to send has to be set when
#			initiating the request object (further down in the config section of this code).
#			If you're using GET you have to set them directly in this URL
# Example:	http://www.myUrl.com/something/vote.php?action=vote&choice=77 	(GET method URL)
#			http://www.myUrl.com/something/vote.php							(POST method URL)
# - - - - -
my $targetUrl = "http://www.inmotionkitesurfing.com/wp-content/plugins/wp-polls/wp-polls.php";

# - - - - -
# Set the maximum number of hits to generate
# - - - - -
my $maxHits = &randomNumber(50,100);

# - - - - -
# Set maximum number of proxy fails in a row before program ends (proxy)
#
# Note:		This only works when running using the proxy mode. Added to avoid the program
#			running for a very long time in case a list of none functional proxy servers
#			is being processed.
#			Each failed requests takes X seconds where X is defined below in the setting
#			$userAgent->timeout(X)
# - - - - -
my $maxSequencedProxyFails = 100;

# - - - - -
# Proxy or no proxy
#
# Values:	'true', 'false'
# Note:		If proxyMode eq 'true' the program will use proxy servers when accessing URLs.
#			There will be done one request (or two if preUrl is defined) per proxy.
#			Note that a list of proxies needs to be loaded from one or more proxy overview
#			web pages.
# - - - - -
my $proxyMode = "true";

# - - - - -
# Avoid using servers that has failed (proxy)
#
# Values:	'true', 'false'
# Note:		If 'true' we don't try to access servers that has failed earlier (servers we find
#			in the failed server log file)
# - - - - -
my $avoidFailedServers = "true";

# - - - - -
# Distinct server lookup (proxy)
#
# Values:	'true', 'false'
# Note:		If "true" all proxy servers that are accessed / tried will be logged
#			and only tried once (log is checked brefore a proxy server is tried)
# - - - - -
my $distinctServer = "true";

# - - - - -
# Proxy list (proxy)
#
# Note:		A list with URLs to sites listing proxy servers
# - - - - -
my $fileProxyList = "proxyList.txt";

# - - - - -
# Logfiles
# - - - - -
my $fileServersSuccess	= "log_serversSuccess.log";
my $fileServersFailed	= "log_serversFailed.log";
my $fileLog				= "log_kHitsGenerator.log";
my $fileProxyLog		= "log_proxyList.log";

# - - - - -
# Lockfile
# - - - - -
my $lockFile = "lockfile_kHitsgenerator.lock";

# - - - - -
# Initialize user agent
# - - - - -
$userAgent->timeout(30);																			# timeout, in seconds
$userAgent->agent('Mozilla/5.0');																	# identify user agent as Mozilla

# - - - - -
# Initiate request object
#
# Note:		If you have chosen POST request method the parameters has to be set here
#
# Example of POST requests:
# 			$request = POST $targetUrl;
# 			$request = POST $targetUrl, [password => 'tmp'];
# 			$request = POST $targetUrl, [action => 'vote', poll_ident => 5, option_id => 7];
# - - - - -
if ($requestMethod eq "GET"){
	$request = HTTP::Request->new(GET => $targetUrl);												# GET request method
} else {
	$request = POST $targetUrl, [vote => 'true', poll_id => 5, poll_5 => 37];
	$postParameterString     = "[vote => 'true', poll_id => 5, poll_5 => 37]";						# for logging purposes only
}
$request->referer($httpReferer);																	# set HTTP referer

# 37 : kari
# 27 : Clementine Bonzom





# -------------------
# main
# -------------------

# lock, to prevent multiple instances running at the same time
&lock;

# initialize proxy url list
&setProxyLists;

# print start
&printStart;

# load failed and used servers
&loadServerLog($fileServersSuccess);
&loadServerLog($fileServersFailed);

# start generating hits
&generateHits;

# print summary
&printResult;

# write proxy log
if ($proxyMode eq "true"){
	&writeProxyLog;
}

# open the lock / delete the lockfile
&unlock;





# -------------------
# sub
# -------------------
sub setProxyLists{

	open (FILE, "<$fileProxyList");
	flock (FILE, 1);																				# share reading, don't allow writing
	while (<FILE>) {
		if ($_ =~ m/^#/sgi || $_ =~ m/^(\s)?$/sgi ){
			next;
		}
		chomp;
		push(@proxyLists, $_);
		$noProxyOverviewPages++;
	}
	close (FILE);
	flock (FILE, 8);																				# unlock file
}


sub generateHits{
	my $i = 0;
	my $y = 0;
	my $content;
	my @servers;
	my $noServers;
	my $noServersThisList;
	my $serverExp = '(\d{2,3}\.\d{2,3}\.\d{2,3}\.\d{2,3}:\d{2,5})';									# regExp to a machine on the Net - IP:portNo
	my $result;
	my $failedSequencedProxyServers = 0;

	# ---
	# Proxy mode
	# ---
	if ($proxyMode eq "true") {
		print "=== Processing lists of proxy servers: ===\n";
		FOREACH: foreach (@proxyLists){																# go through all proxy lists
			print "*** Processing '$_': ***\n";
			$noServersThisList = 0;
			$result = $userAgent->get($_);
			if ($result->is_success){
				$content = $result->content;

				# substitute to support port number in different column in a HTML table
				# - - - - - - - - -
				# Example:
				# - - - - - - - - -
				# <td><span>203.162.31.226</span></td>
             	# <td>80</td>
             	# - - - - - - - - -
				$content =~ s/<\/span>//sgi;
				$content =~ s/(\d+)<\/td>.*?<td>\n?(\d{2})/$1:$2/sgi;
				# do other more site specific parsing of the data
				$content =~ s/&port=/:/sgi;															# xroxy.com specific : "host=101.50.16.30&port=80"
				# nntime.com specific :
				# value="20936700.171.87.2287810848080" onclick="choice()" /></td><td>200.171.87.228<script type="text/javascript">document.write(":"+r+m+r+m)
				# r=5;h=8;g=3;j=9;k=6;y=4;o=1;l=7;x=0;z=2; (found at the top of the page)
				$content =~ s/<script type="text\/javascript">document\.write\(":"/:/sgi;
				$content =~ s/\+r/5/sgi;
				$content =~ s/\+h/8/sgi;
				$content =~ s/\+g/3/sgi;
				$content =~ s/\+j/9/sgi;
				$content =~ s/\+k/6/sgi;
				$content =~ s/\+y/4/sgi;
				$content =~ s/\+o/1/sgi;
				$content =~ s/\+l/7/sgi;
				$content =~ s/\+x/0/sgi;
				$content =~ s/\+z/2/sgi;

			} else {
				push(@proxyOverviewPagesFailed, $_);
				next FOREACH;
			}

			while ($content =~ m/($serverExp)/sg){													# parse content for possible IP addresses
					print "+ $1\n";
					$noProxyServers++;
					$noServersThisList++;
					push(@servers, $1);
					push(@{$proxyOverview{$_}}, $1);												# create a hash array with the URL as the key and the proxy addresses as elements
			}

			if ($noServersThisList == 0){
				push(@{$proxyOverview{$_}}, "NULL");												# if no proxy servers was found for this list we still want to log that, hence we have to add the list URL
			}
		}

		$noServers = $#servers +1;

		if ($startServer eq "random"){																# if we want to start with a random server
			$startServer = &randomNumber(0,($#servers-$maxHits));
		}

		print "=== Testing proxy servers: ===\n";
		FOREACH: foreach (@servers){																# go through all proxy servers
			if ($failedSequencedProxyServers == $maxSequencedProxyFails) {							# exit if we have too many errors in a row
				$maxSequencedProxyFails = -1;														# -1 is used to mark the maximum as reached
				last FOREACH;
			}
			$y++;
			if ($y < $startServer){																	# go next until we have the requested start-server
				next FOREACH;
			} else {
				if ($i < $maxHits) {

					if ($avoidFailedServers eq "true"){
						if ( &isInList($_,@serversFailed) ){										# we have accessed this server from before
							print &timestamp . " : $y/$noServers : FAILED EARLIER : $_ \n";
							next FOREACH;
						}
					}

					if ($distinctServer eq "true"){													# distinct server mode is active
						if ( &isInList($_,@serversAll) ){											# we have accessed this server from before
							print &timestamp . " : $y/$noServers : NOT DISTINCT   : $_ \n";
							next FOREACH;
						}
					}

					$userAgent->proxy(['http', 'ftp'], 'http://'.$_);								# set proxy server (for HTTP and FTP traffic)
					if ($preUrl ne ""){
						$result = $userAgent->get($preUrl);											# request preUrl using GET method (direct with no referer)
					}

					if ( ($preUrl ne "" && $result->is_success) || $preUrl eq ""){
						$result = $userAgent->request($request);									# request object (with referer)
					}

					if ($result->is_success){
						$failedSequencedProxyServers = 0;											# reset failed proxy server counter
						$i++;
						push(@serversSuccess, $_);													# add server to list of accessed/success servers
						print &timestamp . " : $y/$noServers : SUCCESS        : $_ (Hit no $i/$maxHits)\n";
						#print $result->content;
					} else {
						$failedSequencedProxyServers++;												# count all fail servers
						push(@serversFailed, $_);													# add server to list of failed servers
						print &timestamp . " : $y/$noServers : FAILED         : $_ \n";
					}
					push(@serversAll, $_);															# always add server to list over all servers
				}
			}
		}

		&saveServerLog($fileServersSuccess, @serversSuccess);										# save result (success servers)
		&saveServerLog($fileServersFailed, @serversFailed);											# save result (failed servers)

		$actualHits = $i;
	# ---
	# No proxy mode
	# Generate hits directly
	# ---
	} else {																						# don't care about using proxy servers
#		for ($i=0;$i<$maxHits;$i++){
		while ($i < $maxHits){
			#&get($targetUrl);																		# GET mode

			if ($preUrl ne ""){
				$result = $userAgent->get($preUrl);													# request preUrl using GET method (direct with no referer)
			}
	
			if ( ($preUrl ne "" && $result->is_success) || $preUrl eq ""){
				$result = $userAgent->request($request);											# request object (with referer)

				if ($result->is_success){
					$i++;																			# count number of hits generated
					print &timestamp . " : SUCCESS  : (Hit no $i/$maxHits)\n";
				} else {
					$y++;																			# used as counter to avoid to many errors
					print &timestamp . " : FAILED   :\n";
				}
			} else {																				# unable to load preUrl
				$i = $maxHits;																		# exit the loop if we have an error
			}

			if ($y > 5){																			# if we have more than this many unsuccessful requests
				$i = $maxHits;																		# ... we exit the loop
			}

		}

	}
}


sub printStart{
	my $mode;

	if ($proxyMode eq "true"){
		$mode .= "| Proxy mode ($noProxyOverviewPages pages parsed using $maxSequencedProxyFails as max fail limit) ";
	} else {
		$mode .= "| No proxy ";
	}

	if ($distinctServer eq "true"){
		$mode .= "| Distinct servers ";
	} else {
		$mode .= "| All servers ";
	}
	$mode .= "|";

	print "----------------------------\n";
	print "------ kHitsGenerator ------\n";
	print "----------------------------\n";
	print "\n";
	print "Pre URL       : $preUrl\n";
	print "Target URL    : $targetUrl $postParameterString\n";
	print "Request method: $requestMethod\n";
	print "Hits          : $maxHits\n";
	print "Start server  : $startServer\n";
	print "Mode          : $mode\n";
	print "- - - - - - - - - - - - - - \n";

	&writeLog(
		 "=================================================\n"
		."[Starting]\n"
		."HTTP referer    : $httpReferer\n"
		."Pre URL         : $preUrl\n"
		."Target URL      : $targetUrl $postParameterString\n"
		."Request method  : $requestMethod\n"
		."Hits            : $maxHits\n"
		."Start server    : $startServer\n"
		."Mode            : $mode"
	);
}


sub printResult{
	print "- - - - - - - - - - - - - - \n";
	print "URL                : $targetUrl\n";
	print "Actual hits        : $actualHits\n";
	print "Wanted hits        : $maxHits\n";
	if ($proxyMode eq "true"){
		print "Proxy servers found: $noProxyServers\n";
	}
	if ($maxSequencedProxyFails == -1){
		print "Program terminated due to too many proxy servers failing!\n"
	} else {
		$maxSequencedProxyFails = 0;
	}
	print "----------------------------\n";
	&writeLog(
		 "\n"
		."Actual hits     : $actualHits/$maxHits\n"
		."No. proxy srv   : $noProxyServers\n"
		."Proxy fail exit : $maxSequencedProxyFails\n"
		."[Stopping]      : ================================================="
	);
}


sub loadServerLog{
	my $fileLog = $_[0];
	my $line;

	open (FILE, "<$fileLog") || print("ERROR: Could not open '$fileLog'\n");
	while ($line = <FILE>){
		$line =~ s/\n//;
		if ($fileLog =~ m/Success/){
			push(@serversSuccess, $line);
		} elsif ($fileLog =~ m/Failed/) {
			push(@serversFailed, $line);
		}
		push(@serversAll, $line);
	}
	close (FILE);
}


sub saveServerLog{
	my ($fileLog, @servers) = @_;

	open (FILE, ">$fileLog") || print("ERROR: Could not open '$fileLog'\n");
	foreach (@servers){
		print FILE $_ ."\n";
	}
	close (FILE);
}


sub isInList{
	my ($entry, @list) = @_;

	foreach (sort @list){
		if ($entry eq $_){
			return 1;																				# we do have an entry
		}
	}
	return 0;																						# didn't find anything
}


sub randomNumber{
	my $low   = $_[0];
	my $heigh = $_[1];
	my $random;

	$heigh++;																						# add 1 to heigh so rand will return the right number
	# example:
	# low = 10, heigh = 21
	# heigh = 11
	$heigh = $heigh - $low;

	# random in between 0-11
	$random = rand($heigh);
	#random in between 10-21
	$random += $low;

	return int($random);																			# using int the number will be rounded, like floor()
}


sub writeLog{
	my $line = $_[0];
	my $timestamp = &timestamp;

	open (FILE, ">>$fileLog") || print("ERROR: Could not open '$fileLog'\n");
	print FILE $timestamp . " : " . $line ."\n";
	close (FILE);
}


sub writeProxyLog{
	my $timestamp = &timestamp;
	my $key;
	my $i;
	my $noTotalServers;
	my $noSuccess;
	my $noFailed;
	my $noProcessed;
	my $noNotProcessed;
	my $noTotalServers_divisor;
	my $noProcessed_divisor;
	my @matches;

	open (FILE, ">>$fileProxyLog") || print("ERROR: Could not open '$fileProxyLog'\n");
	print FILE "$timestamp : =================================================\n";

	# $key						= the URL to the proxy overview site
	# $proxyOverview{$key}[$i]	= one specific proxy server
	for $key ( sort{ $proxyOverview{$a} cmp $proxyOverview{$b} }  keys %proxyOverview ) {
		$noTotalServers = $#{$proxyOverview{$key}}+1;
		$noSuccess = 0;
		$noFailed = 0;

		for $i (0 .. $#{$proxyOverview{$key}}){
			@matches = grep( /^$proxyOverview{$key}[$i]$/, @serversSuccess );
			if ( scalar(@matches)>0 ){
				$noSuccess++;
			}
			@matches = grep( /^$proxyOverview{$key}[$i]$/, @serversFailed );
			if ( scalar(@matches)>0 ){
				$noFailed++;
			}
			$noProcessed = $noFailed + $noSuccess;
			$noNotProcessed = $noTotalServers - $noProcessed;
		}

		$noTotalServers_divisor = $noTotalServers;													# this number will never be zero since an empty url / proxy overview site is stored with one element named "NULL"
		$noProcessed_divisor	= $noProcessed;														# this might very well be zero though

		if ($noProcessed == 0){
			$noProcessed_divisor	= 1;
		}

		if ($noTotalServers == 1 && $proxyOverview{$key}[0] eq "NULL"){
			$noTotalServers			= 0;															# has to set this to 0 as it has been "wrongly" set to 0+1 at the start of this subroutine
			$noNotProcessed			= 0;															# ... hence this has to be reset as well
		}

		print FILE "| total: $noTotalServers | processed: $noProcessed ("
			. int(($noProcessed/$noTotalServers_divisor)*100 + 0.5)
			. "%) | not processed: $noNotProcessed ("
			. int(($noNotProcessed/$noTotalServers_divisor)*100 + 0.5)
			. "%) | success: $noSuccess ("
			. int(($noSuccess/$noProcessed_divisor)*100 + 0.5)
			. "%) | failed: $noFailed ("
			. int(($noFailed/$noProcessed_divisor)*100 + 0.5)
			. "%) | $key |\n";
	}

	foreach (@proxyOverviewPagesFailed){
		print FILE "GET FAILED : $_\n";
	}

	close (FILE);
}


sub timestamp{
	my $time = time;
	my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime($time);
	my $timestamp;

	$year = $year+1900;
	$mon = $mon+1;

	# puts a zero in front of one digit.
	$sec =~ s/^(\d){1}$/0$1/;
	$min =~ s/^(\d){1}$/0$1/;
	$hour =~ s/^(\d){1}$/0$1/;
	$mday =~ s/^(\d){1}$/0$1/;
	$mon =~ s/^(\d){1}$/0$1/;

	# 19991109_182455
	$timestamp = $year . $mon . $mday . "_" . $hour . $min . $sec;

	return $timestamp;
}


sub lock{
	if (-e $lockFile){
		&writeLog(
			 "\n"
			."Program is still running. New instance is stopped.\n"
			."                : ================================================="
		);
		exit;
	} else {
		open (FILE, ">$lockFile") || print("ERROR: Could not open '$lockFile'");
		flock (FILE, 2);
		print FILE "";
		close (FILE);
		flock (FILE, 8);
	}
}


sub unlock{
	if (-e $lockFile){
		unlink $lockFile;
	}
}